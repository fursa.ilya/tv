package com.freelance.tv.generator;

import com.freelance.tv.model.Channel;

import java.util.*;

/**
 * Класс помогающий произвести автонастройку Телевизора
 * Генерирует рандомный список каналов
 */
public class ChannelUtil {
    private static final int CHANNELS_NUMBER = 5; //Всего каналов на телевизоре
    private static List<Channel> allChannels = new ArrayList<>();
    private static Random rnd = new Random();
    //Инциализируем список каналов
    static {
       allChannels.add(new Channel("MTV", "Музыка, Развлечения", true));
       allChannels.add(new Channel("Animal Planet", "Животные и природа", true));
       allChannels.add(new Channel("МузТВ", "Музыка", true));
       allChannels.add(new Channel("Nickelodeon", "Мультфильмы", true));
       allChannels.add(new Channel("Рен-ТВ", "Конспирология, Альтернативная наука", true));
       allChannels.add(new Channel("Netflix", "Фильмы, Сериалы", true));
       allChannels.add(new Channel("AMC", "Фильмы, Сериалы, Ток-Шоу", true));
       allChannels.add(new Channel("Россия-24", "Новости", true));
       allChannels.add(new Channel("НТВ+", "Спорт", true));
       allChannels.add(new Channel("Эврика", "Наука", true));
       allChannels.add(new Channel("Naked Science", "Наука", true));
       allChannels.add(new Channel("Cosmos TV", "Космос, Астрономия", true));
       allChannels.add(new Channel("МедМакс", "Медицина", true));
       allChannels.add(new Channel("Вегетарианское ТВ", "Еда", true));
       allChannels.add(new Channel("Спас", "Православие", true));
    }

    //Получаем n неповторяющихся каналов из списка
    public static List<Channel> getTvChannels() throws InterruptedException {
        int numberOfChannels = (int) (Math.random() * allChannels.size());
        System.out.println("[+]На ТВ установлено: " + numberOfChannels + " каналов");
        List<Channel> newChannels = new ArrayList<>();
        if(numberOfChannels > 0) {
            for (int i = 0; i < numberOfChannels; i++) {
                Channel newChannel = allChannels.get(i);
                newChannels.add(newChannel);
            }
        }

        return newChannels;
    }


}

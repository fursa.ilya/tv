package com.freelance.tv;

import com.freelance.tv.model.Channel;
import com.freelance.tv.model.Televisor;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Televisor televisor = new Televisor("LG", 0, false); //Создаем новый телевизор
        televisor.turnOn(); //Включаем телевизор
        if(televisor.getState()) { //если телевизор включен
            televisor.setup(); //Производим его настройку
            televisor.printCurrentChannelName(); //Текущий канал который смотрит пользователь
            televisor.nextChannel(); //Переключаем канал с помощью кнопки вперед
            televisor.getAllChannels().stream()
                    .forEach(channel -> System.out.println(channel.getTitle())); //Выводим все каналы
            televisor.printCurrentChannelName();
            televisor.soundHigh();
            televisor.soundHigh(); //Прибавляем звук дважды
            System.out.println("Текущий уровень звука: " + televisor.getVolume());
            televisor.mute(); //Выключаем звук на телевизоре
            System.out.println("Текущий уровень звука: " + televisor.getVolume());
            televisor.soundHigh();
            televisor.soundHigh();
            televisor.soundHigh(); //Прибавляем звук
            System.out.println("Текущий уровень звука: " + televisor.getVolume());
            televisor.soundLow();
            System.out.println("Текущий уровень звука: " + televisor.getVolume());
            televisor.addChannel(new Channel("Тестовый канал", "Тест", false));

            System.out.println("Текущий включенный канал: " + televisor.getChannelEnabled().getTitle() + " enabled: " + televisor.getChannelEnabled().isEnabled());
        } else {
            System.out.println("Телевизор выключен. Пожалуйста включите командой turnOn");
        }
    }
}

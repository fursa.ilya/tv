package com.freelance.tv.model;

import com.freelance.tv.model.Channel;

import java.util.List;

/**
 * Интерфейс описывающий
 * контракт который должен реализовывать телевизор
 * Объект который можно смотреть Watch able
 */
public interface Watchable {

    boolean turnOn(); //Включение телевизора

    boolean turnOff(); //Выключение телевизора

    void switchChannel(int number); //переключение канала на цифровой панели

    boolean nextChannel(); //Следующий канал

    boolean previousChannel(); //Предыдущий канал

    void soundLow(); //Уменьшение звука

    void soundHigh(); //Увеличение звука

    void mute(); //Выключить звук

    List<Channel> getAllChannels(); //Получить список всех каналов

    void addChannel(Channel newChannel); //Добавить канал

    void deleteChannel(Channel channel); //Удалить канал

    void setup() throws InterruptedException; //Произвести автонастройку

    void printCurrentChannelName(); //Напечатать название текущего просматриваемого канала

    Channel getChannelEnabled(); //Найти канал который просматривает пользователь в текущий момент
}

package com.freelance.tv.model;

/**
 * Телевизионный канал
 * @param title - название канала
 * @param theme - тематика канала(Спорт, Политика, Музыка и тд)
 * @param isEnabled - вещает ли в текущий момент?
 */
public class Channel {
    private String title;
    private String theme;
    private boolean isEnabled;

    public Channel(String title, String theme, boolean isEnabled) {
        this.title = title;
        this.theme = theme;
        this.isEnabled = isEnabled;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    @Override
    public String toString() {
        return "Channel [title: " + title + " theme: " + theme + " isEnabled: " + isEnabled + "]";
    }
}

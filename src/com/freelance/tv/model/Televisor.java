package com.freelance.tv.model;

import com.freelance.tv.generator.ChannelUtil;

import java.util.ArrayList;
import java.util.List;

public class Televisor implements Watchable {
    private int volume; //Уровень громкости от 0 до 10
    private boolean state; //Включен или выключен(Текущее состояние)
    private List<Channel> channels = new ArrayList<>();
    private Channel currentChannel; //Текущий канал который смотрит пользователь. По умолчанию 0 канал
    private int currentChannelPosition; //Номер канала который смотрит пользователь
    private String tvName;

    public Televisor(String tvName, int volume, boolean state) {
        this.tvName = tvName;
        this.volume = volume;
        this.state = state;
    }

    /**
     * Получить название телевизора
     * @return tvName
     */
    public String getTvName() {
        return tvName;
    }

    /**
     * Метод описывает включение телевизора.
     * Ставит переменную состояния в true - включен
     * @return state
     */
    @Override
    public boolean turnOn() {
        state = true;
        return state;
    }

    /**
     * Выключение телевизора
     * @return state - false
     */
    @Override
    public boolean turnOff() {
        state = false;
        return state;
    }
    //Получаем текущее состояние телевизора
    public boolean getState() {
        return state;
    }
    /**
     * Делаем проверку, попадает ли число в диапазон доступных каналов,
     * если да, то переключаем канал и устанавливаем текущую позицию канала
     * @param number
     */
    @Override
    public void switchChannel(int number) {
        if(number >= 0 && number <= channels.size()) {
            Channel newChannel = channels.get(number);
            currentChannelPosition = number;
            currentChannel = newChannel;
            currentChannel.setEnabled(true);
        } else {
            System.out.println("[!]На этой кнопке нет канала");
        }

    }

    /**
     * Переключаем канал кнопкой вперед
     * @return true - если канал был переключен
     */
    @Override
    public boolean nextChannel() {
        currentChannelPosition++;
        Channel newChannel = channels.get(currentChannelPosition);
        currentChannelPosition--;
        channels.get(currentChannelPosition).setEnabled(false); //Выключаем канал после переключения на следующий
        currentChannel = newChannel;
        currentChannel.setEnabled(true); //Устанавливаем канал который в данный момент смотрит пользователь
        return true;
    }

    //получаем уровень звука
    public int getVolume() {
        return volume;
    }

    /**
     * Переключаем канал кнопкой назад
     * @return true - если был переключен
     */
    @Override
    public boolean previousChannel() {
        currentChannelPosition--;
        if(currentChannelPosition >= 0 && currentChannelPosition <= channels.size()) {
            Channel newChannel = channels.get(currentChannelPosition);
            channels.get(currentChannelPosition++).setEnabled(false); //Выключаем канал
            currentChannel = newChannel;
            currentChannel.setEnabled(true);
            return true;
        }

        return false;
    }

    /**
     * Уменьшаем звук, предварительно проверив значение на попадание в диапазон.
     * Минимальный уровень звука 0
     * Максимальный 10
     */
    @Override
    public void soundLow() {
        if(volume >= 0 && volume <= 10) {
            volume--;
        }
    }

    /**
     * Увеличиваем звук, предварительно проверив значение на попадание в диапазон.
     * Минимальный уровень звука 0
     * Максимальный 10
     */
    @Override
    public void soundHigh() {
        if(volume >= 0 && volume <= 10) {
            volume++;
        }
    }

    /**
     * Выключаем звук
     */
    @Override
    public void mute() {
        volume = 0;
    }

    /**
     * Получаем список всех каналов
     * @return
     */
    @Override
    public List<Channel> getAllChannels() {
        return channels;
    }

    /**
     * Добавляем новый канал
     * @param newChannel
     */
    @Override
    public void addChannel(Channel newChannel) {
        channels.add(newChannel);
    }

    /**
     * Удаляем канал
     * @param channel
     */
    @Override
    public void deleteChannel(Channel channel) {
        channels.remove(channel);
    }

    /**
     * Проводим автоматическую настройку каналов
     * Устанавливает текущий канал на 0
     */
    @Override
    public void setup() throws InterruptedException {
        System.out.println("[+]Производится автонастройка каналов ТВ");
        List<Channel> tmp = ChannelUtil.getTvChannels();
        if(tmp.size() > 0) {
            channels.addAll(tmp);
            currentChannel = channels.get(0);
        }
    }

    /**
     * Печатаем имя текущего канала
     */
    @Override
    public void printCurrentChannelName() {
        System.out.println("[+]" + currentChannel.getTitle());
    }

    @Override
    public Channel getChannelEnabled() {
        for (int i = 0; i < channels.size(); i++) {
            if(channels.get(i).isEnabled()) {
                return channels.get(i);
            }
        }

        return currentChannel;
    }
}
